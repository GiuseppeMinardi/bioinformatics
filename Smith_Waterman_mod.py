"""
Giusepe Minardi - 197754
University of Trento

This file contains an implementation of the *Smith- Waterman* algorithm modified
for the exam.  this program returns the best soring alignment using the gap
weight equal to 5 and the resulting alignment with no more than 10% gaps with
respect to the alignment lenght.

Feb 2019
"""


import numpy as np
import argparse
import os
import sys

#===============================================================================
# Standard Variables
match = 3
mismatch = -3
gap = -5

# matchscoreinor scores to print
minscore = 1
# matchscoreinor lenght to print
minlength = 1
# matchscoreinimum number of resolt
numresult = 1
#===============================================================================
def params():
    """
    This funcion uses argparse in order to retrieve inputs
    """

    obj_parse_args = argparse.ArgumentParser(description="""gapscoreiven two
    sequences or a file containing sequences return the alignments.
        The sequences in the input file have to be tab separated""")

    obj_parse_args.add_argument("--seq1",
            type = str,
            help = "The first sequence to align")

    obj_parse_args.add_argument("--seq2",
            type = str,
            help = "The second sequence to align")

    obj_parse_args.add_argument("-i",
            "--input",type=str,help="The file in input")

    obj_parse_args.add_argument("-m",
            "--match",
            default=match,
            type=float,
            required=False,
            help="score of the match (default = {})".format(match))

    obj_parse_args.add_argument("-s",
            "--mismatch",
            default=mismatch,
            type = float,
            required = False,
            help = "score of the mismatch (default = {})".format(mismatch))

    obj_parse_args.add_argument("-g",
            "--gap",
            default = gap,
            type = float,
            required = False,
            help = "score of the gap (default = {})".format(gap))

    obj_parse_args.add_argument("--minscore",
            default = minscore,
            type = float,
            required = False,
            help = "The minimum score")

    obj_parse_args.add_argument("--minlength",
            default = minlength,
            type = int,
            required = False,
            help = "The minimum length")

    obj_parse_args.add_argument("--numresult",
            default = numresult,
            type = int,
            required = False,
            help = "The number of alignments to return")

    args = obj_parse_args.parse_args()
    return args

def check_params(parameters):
    """
    Check parameter given in input
    """
    parameters.seqs = []
    input_f = False
    if parameters.input is not None:
        if os.path.exists(parameters.input):
            if os.path.isfile(parameters.input):
                size = len(parameters.seqs)
                parameters.seqs += [tuple(line.strip().split("\t")) for line
                    in open(parameters.input) if len(line.strip().split("\t"))==2]
                input_f = True
                if len(parameters.seqs) <= size:
                    print("""the file could be empty or wrongly formatted: \nseq1\tseq2
                        \nseq3\tseq4\n...\n(tab separated)""")
            else:
                print("input is not a file")
        else:
            print("input file doesn't exist")

    if ((parameters.seq1 is not None) and (parameters.seq2 is not None)):
            parameters.seqs.append((parameters.seq1,parameters.seq2))

    if ((parameters.seq1 is None) or  (parameters.seq2 is None)) and input_f is False:
        print("You have to give in input at least two sequences or a file of sequences!")
        if not parameters.seqs:
            print("You did not give me anything...")
            sys.exit(1)

    if parameters.match <= 0:
        print("provided value for matches is not correct, default will be used instead")
        parameters.match = match

    if parameters.mismatch > 0:
        print("Provided value for mismatches is not correct, default will be used instead")
        parameters.mismatch= mismatch

    if parameters.gap > 0:
        print("Provided value for gaps is not correct, default will be used instead")
        parameters.gap = gap

    if parameters.minscore is not None:
        if parameters.minscore < 0:
            print("""Provided value for minimium score is negative, no minimum score will be used""")
            parameters.minscore = minscore

    if parameters.minlength is not None:
        if parameters.minlength < 0:
            print("""Provided value for minimium length is negative, no minimum length will be used""")
            parameters.minlength = minlenght


    if parameters.numresult is not None:
        if parameters.numresult < 0:
            print("""Provided value for the minimum number of results is less than 1, it will be printed just one instance""")
            parameters.numresult = numresult

    return parameters
#================================================================================
def SubMatrix(a, b, matchscore, mismatchscore):
    """
    This function define the mach/mismatch penalty
    """

    if a == b:
        return (matchscore)
    elif a != b:
        return (mismatchscore)

    return
#================================================================================
def CheckParameters(matchscore , mismatchscore , gapscore, ParameterList):
    """
    Check parameters
    """

    if matchscore < 0:
        raise ValueError("match parameter cannot be less than zero.")
    if mismatchscore > 0:
        raise ValueError("mismatch parameter cannot be greater than zero.")
    if gapscore > 0:
        raise ValueError("gap parameter cannot be greater than zero.")
    if ParameterList[1] < 0:
        raise ValueError("matchscoreinimum lengh cannot be negative.")
    if ParameterList[2] < 1:
        raise ValueError("You must print at least 1 result")
#=============================================================================
def SmithWaterman(seq1,
        seq2,
        matchscore = match,
        mismatchscore = mismatch,
        gapscore = gap,
        FilterParameters =[1, 1, 1]):
    """
    This function create the alinment matrix and the trace matrix.
    Calls Submatchscoreatrix and the Traceback.
    """

    if __name__ != '__main__':
        # If this function is imported parameters must be checked
        CheckParameters(matchscore, mismatchscore,gapscore, FilterParameters)

    matrix = np.zeros((len(seq1)+1, len(seq2)+1))
    trace = np.zeros((len(seq1)+1, len(seq2)+1))


    values=[0,0,0,0,0]

    for i in range(1,len(seq1)+1):
        for j in range(1,len(seq2)+1):
            values[1] = matrix[i-1,j-1] + SubMatrix(seq1[i-1],seq2[j-1],matchscore,mismatchscore)
            values[2] = matrix[i-1,j]+gapscore
            values[3] = matrix[i,j-1]+gapscore

            matrix[i,j] = max(values)

            if max(values) != 0:
                trace[i,j] = values.index(max(values))

    TraceBackInizialization(matrix, trace, seq1, seq2, FilterParameters)
#=============================================================================
def TraceBackInizialization(matrix, trace, seq1, seq2, FilterParameters):
    """
    This function find the maximum in the matrix and calls the
    recursive traceback function
    """
    aligns = []

    for score in np.unique(matrix):
        if score != 0:
            row ,column = np.where(matrix == score)
            for j in range(0,len(row)):
                alng = TraceBack_rec(trace,
                                     row[j],
                                     column[j],
                                     seq1,
                                     seq2,
                                     out=['','',''])

                aligns.append((score, alng[0], alng[1], alng[2]))

    aligns.sort(reverse=True)
    FilterPrint(aligns, FilterParameters)
#=============================================================================
def TraceBack_rec(trace, row, column,seq1,seq2, out):
    """
    A recursive function that defines the alignment
    """
    if trace[row][column] == 1:
        out[0] += seq1[row-1]
        out[1] += seq2[column-1]
        if seq1[row-1] == seq2[column-1]:
            out[2] += '|'
        else:
            out[2] += ':'
        return TraceBack_rec(trace,row-1,column-1,seq1,seq2,out=out)

    elif trace[row][column] == 2:
        out[0] += seq1[row-1]
        out[1] += '-'
        out[2] += ' '
        return TraceBack_rec(trace,row-1,column,seq1,seq2,out=out)

    elif trace[row][column] == 3:
        out[0] += '-'
        out[1] += seq2[column-1]
        out[2] += ' '
        return TraceBack_rec(trace,row,column-1,seq1,seq2,out=out)

    else:
        return out
#=============================================================================
def FilterPrint(AlignmentList,ParameterList):
    """
    This function filter the list by the parameters.
    """
    out = []

    i = 0
    while i < ParameterList[2] and i < len(AlignmentList):
        GapPerc = AlignmentList[i][3].count(' ') / len(AlignmentList[i][3])

        if AlignmentList[i][0] >= ParameterList[0] and len(AlignmentList[i][1]) >= ParameterList[1]:
            if GapPerc <= 0.1:
                print("""=================\nOriginal Sequences:\n{}\n{}
                        """.format(seq[0], seq[1]))

                print('Alignment score: ',AlignmentList[i][0])
                print("{}\n{}\n{}".format(
                    AlignmentList[i][1][::-1],
                    AlignmentList[i][3][::-1],
                    AlignmentList[i][2][::-1]))
                print()
                print("""Found:\n{}\tMatch\n{}\tGap\n{}\tAlignment lenght\n{}\tGap %
                    """.format(AlignmentList[i][3].count(':'),
                               AlignmentList[i][3].count(' '),
                               len(AlignmentList[i][3]),
                               round(GapPerc*100,3)))

        i += 1
#====================================================================

if __name__=="__main__":
    args=params()
    args=check_params(args)
    for seq in args.seqs:
        SmithWaterman(seq[0],
                seq[1],
                matchscore = args.match,
                mismatchscore = args.mismatch,
                gapscore = args.gap,
                FilterParameters = [args.minscore,
                    args.minlength,
                    args.numresult])
#================================================================================
