# Bioinformatics

Repository for the implementation of the **Smith-Waterman** algorithm.
Bioinformatics course, University of Trento, (Master's degree in Quantitative and Computational Biology).

This program can be called from the command line or imported into another python file.

## Usage

### Command line
`python3 SmithWaterman.py --seq1 <sequence> --seq2 <sequence>`

`python3 SmithWaterman.py -i sequencefile.txt`

- -s mismatch penalty
- -g gap penalty
- -m match score
- --minscore minimum score to print
- --minlength minimum length to print
- --numresult number of substring to print

### Imported module
```
   import Smith_Waterman as sw

   seq1 = 'TGTTACGG'
   seq2 = 'GGTTGACTA'

   sw.SmithWaterman(seq1,seq2)
```
Other parameters are set to default but it it possible to give it to the function:

- matcscore = match score
- mismatchscore = mismatch penalty
- gapscore = gap penalty
- FilterParameters = list containing:
  - minscore
  - minlenght
  - numesult

Example with different settings:
```
   import Smith_Waterman as sw

   seq1 = 'TGTTACGG'
   seq2 = 'GGTTGACTA'

   sw.SmithWaterman(seq1,seq2, M = 3, Ms = -2, G = -5, F = [5, 4, 20])
```
In this example an alignment is performed, match sore is 3, mismatch and gap score are -3 and -5.
Are printed the first 20 alignment with at least score = 4 and long 4 or more characters.
